import requests
import random
import time
start_time = time.time()

for i in range(100):
    sample_file = open("coursera.txt", 'r')
    text = sample_file.read()
    p = {'x':random.randrange(1,10000000), 'y':random.randrange(1,1000000000),'text':text}
    r = requests.post('http://localhost:16799/check', params = p)
    #print r.json()
    gto = r.json()['goto']
    r2 = requests.get("http://localhost:16799/check/result_keyword/{}".format(gto))
    print r2.json()
print("--- %s seconds ---" % (time.time() - start_time))
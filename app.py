from __future__ import absolute_import
from os import path, environ
import json
import rake
import operator
import requests
from flask import Flask, Blueprint, abort, jsonify, request, session
import settings
from celery import Celery
app = Flask(__name__)
app.config.from_object(settings)
def make_celery(app):
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

celery = make_celery(app)
rake_object = rake.Rake("SmartStoplist.txt", 5, 3, 4)

# @celery.task(name="tasks.add")
# def add(x, y):
#     return x + y

@celery.task(name="tasks.text_process")
def text_process(text):
    keywords = rake_object.run(text)
    return keywords

"""
@app.route("/test")
def hello_world(x=16, y=16):
    x = int(request.args.get("x", x))
    y = int(request.args.get("y", y))
    res = add.apply_async((x, y))
    context = {"id": res.task_id, "x": x, "y": y}
    result = "add((x){}, (y){})".format(context['x'], context['y'])
    goto = "{}".format(context['id'])
    return jsonify(result=result, goto=goto)

@app.route("/test/result/<task_id>")
def show_result(task_id):
    retval = add.AsyncResult(task_id).get(timeout=1.0)
    return repr(retval)
"""
@app.route('/check', methods=['POST'])
def index(text="hello",x=16,y=16):
    x = int(request.args.get("x", x))
    y = int(request.args.get("y", y))
    text=str(request.args.get("text", text))
    res = text_process.apply_async([text,])
    context = {"id": res.task_id, "x": x, "y": y,"text":text}
    #url = request.form['url']
    #r = requests.get(url)
    #print(r.text)
    result = "keyword((x){}, (y){}, (text){})".format(context['x'], context['y'],context['text'])
    goto = "{}".format(context['id'])
    return jsonify(result=result, goto=goto)
    #return jsonify(text, goto=goto)
    #return text

@app.route("/check/result_keyword/<task_id>", methods=['GET'])
def show_text_process_result(task_id):
    retval = text_process.AsyncResult(task_id).get(timeout=1.0)
    print retval
    return jsonify(retval)


if __name__ == "__main__":
    port = int(environ.get("PORT", 16799))
    app.run(host='0.0.0.0', port=port, debug=True)
